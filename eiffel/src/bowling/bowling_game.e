note
	description: "Mantiene il punteggio di una partita di bowling"
	author: "Garion"
	date: "$Date$"
	revision: "$Revision$"

class BOWLING_GAME

	create make										-- costruttore: "make"

	feature {NONE}									-- feature private
		rolls: ARRAY[INTEGER]						-- lista dei tiri

		sum_of_rolls: INTEGER
		do
			Result := 0
			across rolls as r
			loop
				Result := Result + r.item
			end
		end

	feature {ANY}									-- feature pubbliche
		max_rolls: INTEGER							-- massimo lanci (21)
		rolls_count : INTEGER						-- totale corrente dei tiri effettuati


		--------	costruttore		--------
		feature make
		do
			max_rolls := 21
			create rolls.make_filled(0, 0, max_rolls)
			rolls_count := 0

		ensure
			rolls_21: max_rolls = 21
			rolls_0: rolls_count = 0
			score_0: get_score = 0

		end


		---------	effettua un lancio	----------
		feature roll(pins: INTEGER)
		require
			valid_pin_in: pins >= 0 and pins <= 10
			valid_roll: rolls_count >= 0 and rolls_count <= max_rolls

		do
			rolls.put (pins, rolls_count)
			rolls_count := rolls_count + 1

		ensure
			one_roll: rolls_count = old rolls_count + 1
			valid_score: get_score >= 0 and get_score <= (30 * rolls_count)
			score_increment: get_score >= old get_score

		end -- do


		---------	calcola punteggio attuale	----------
		feature get_score: INTEGER
		require
			valid_frames: rolls_count >= 0 and rolls_count <= 21

		local
			i : INTEGER
			frame_index : INTEGER
			this_roll : INTEGER
			next_roll : INTEGER
			next_next_roll : INTEGER

		do
			from
				i := 0
				until
				i >= 10
			loop
				this_roll := rolls.item(frame_index)
				next_roll := rolls.item(frame_index + 1)
				next_next_roll := rolls.item(frame_index + 2)

				-- strike
				if this_roll = 10 then
					Result := Result + 10 + next_roll + next_next_roll
					frame_index := frame_index + 1

				-- spare
				elseif this_roll + next_roll = 10 then
					Result := Result + 10 + next_next_roll
					frame_index := frame_index + 2

				-- tiro normale
				else
					Result := Result + this_roll + next_roll
					frame_index := frame_index + 2
				end	-- if
				
				i := i + 1
			end	-- loop

			ensure
				score_0_300: Result >= sum_of_rolls and Result <= (30 * rolls_count)

		end	-- do


		----------	invarianti	----------
		invariant
			frames_21: rolls_count >= 0 and rolls_count <= max_rolls
			valid_score: get_score >= 0 and get_score <= (30 * rolls_count)
			valid_rolls: across rolls as r all r.item >= 0 and r.item <= 10 end
end
