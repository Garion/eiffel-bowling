note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET

-- ridefinisce funzione "set up"
redefine on_prepare end

	-- attributi privati
	feature  {NONE}
	    game: BOWLING_GAME

	-- before every test
	feature on_prepare
    do
   		create game.make
   	end

	-- utility
	roll_many(lanci: INTEGER; pins: INTEGER)

	local
		i : INTEGER

	do
		from
			i := 0
		until
			i >= lanci
		loop
			i := i + 1
			game.roll(pins)
		end
	end

	-- lancia tutti 0
	game_test_gutter
	do
		roll_many(20, 0)
		check
			game.get_score = 0
		end
	end

	-- lancia tutti 1
	game_test_all_ones
	do
		roll_many(20, 1)
		check
			game.get_score = 20
		end
	end

	-- lancia uno spare, due 3 e tanti 0
	game_test_one_spare
	do
		roll_many(2, 5)
		game.roll (3)
		roll_many(17, 0)
		check
			game.get_score = 16
		end
	end

	-- lancia uno strike e altri random
	game_test_one_strike
	do
		game.roll(10)
		game.roll(3)
		game.roll(4)
		roll_many(16, 0)
		check
			game.get_score = 24
		end
	end

	-- lancia 12 strike
	game_test_perfect_game
	do
		roll_many(12, 10)
		check
			game.get_score = 300
		end
	end

	-- lancia 9 strike e uno spare
	game_test_last_spare
	do
		roll_many(9, 10)
		roll_many(2, 5)
		game.roll(10)
		check
			game.get_score = 275
		end
	end

end
